import { Component, OnInit } from '@angular/core';
import { Report } from '../models/report';
import { ReporteService } from '../service/reporte.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  reporte: Report = new Report();

  constructor(private reporteServicio: ReporteService) { }

  ngOnInit(): void {
    this.reporteServicio.test()
      .subscribe(
        res => {
          console.log('ok', res);
        },
        err => {
          console.log('bad request', err);
        }
      );
  }

  guardarReporte() {
    this.reporteServicio.crearReporte(this.reporte).subscribe(datos => {
      alert(" Se agrego con exito");
      console.log('llego', datos);
    })

  }

}
