import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Report } from '../models/report'

@Injectable({
  providedIn: 'root'
})
export class ReporteService {


  path = 'http://localhost:8080';

  public headersJson = new HttpHeaders({
    'Content-Type': 'application/json',
    'accept': '*/*',
  });

  constructor(private http: HttpClient) {

  }

  public crearReporte(reporte: Report) {
    const pathTest = [this.path, 'reporte', 'post'].join('/');
    return this.http.post<Report>(pathTest, reporte);

  }


  public test() {
    const pathTest = [this.path, 'reporte', 'test'].join('/');
    return this.http.get(pathTest, { headers: this.headersJson });
  }


}
