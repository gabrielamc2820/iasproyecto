import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './report/report.component';
import { CalculateComponent } from './calculate/calculate.component';

const routes: Routes = [
  { path:'reporte', component: ReportComponent },
  { path:'calculo', component: CalculateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
