package com.ias.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ias.modelo.Usuario;
import com.ias.repositorio.UsuarioRepositorio;


@Service
public class UsuarioServicio {
	
	@Autowired
	private UsuarioRepositorio repositorio;
	
	public Usuario guardar(Usuario usuario) throws Exception{
		
		return repositorio.save(usuario);
		
	}
	
	@Transactional(readOnly = true)
	public Usuario findById(Long id) {
		return repositorio.findById(id).orElse(null);
	}
	
	/*public Usuario obtenerUsuario(Long id) throws Exception{
		
		return repositorio.getOne(id);
		
	}*/
	
	//public Usuario obtenerUsuarioIdentificacion(String identificacion) throws Exception{
		
		//return repositorio.findByIdentificacion(identificacion);
	
	//}
	

}
