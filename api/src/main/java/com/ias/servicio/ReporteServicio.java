package com.ias.servicio;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ias.modelo.Servicio;
import com.ias.modelo.Usuario;
import com.ias.repositorio.ServicioRepositorio;

@Service
public class ReporteServicio {
	
	@Autowired
	private ServicioRepositorio servicioRrepositorio;
	
	/*public Servicio guardar(String identificacion, Servicio servicio) throws Exception{
		
		Usuario usuario = usuarioServicio.obtenerUsuarioIdentificacion(identificacion);
		if(usuario == null) {
			throw new Exception("No existe un usuario con la identificación digitada");
		}
		servicio.setIdUsuario(usuario);
		return repositorio.save(servicio);
	}*/
	
	@Transactional
	public Servicio save(Servicio reporteModelo) throws Exception {
		return servicioRrepositorio.save(reporteModelo);
	}

}
