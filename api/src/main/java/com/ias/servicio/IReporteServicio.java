package com.ias.servicio;

import com.ias.modelo.Servicio;

public interface IReporteServicio {
	
	/**
	 * Crea un reporte
	 * @param reporte Servicio
	 * @return Servicio
	 */
	public Servicio save(Servicio reporte);

}
