package com.ias.servicio;

import com.ias.modelo.Usuario;

public interface IUsuarioServicio {
	
	/**
	 * Encuentra un usuario por la identificacion
	 * @param identificacion Sring
	 * @return Usuario
	 */
	public Usuario findById(String identificacion);

}
