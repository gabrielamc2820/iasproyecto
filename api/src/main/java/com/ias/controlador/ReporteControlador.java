package com.ias.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ias.modelo.Servicio;
import com.ias.servicio.ReporteServicio;

@CrossOrigin(origins = "*")//todas direcciones
//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/reporte")
public class ReporteControlador {
	
	//@Autowired(required = false)
	//private UsuarioServicio usuarioServicio;
	
	@Autowired(required = false)
	private ReporteServicio reporteServicio;
	
	
	@PostMapping(value = "post")
	public ResponseEntity<Servicio> agregar(@RequestBody Servicio reporte) throws Exception{
		Servicio reporteG=reporteServicio.save(reporte);
		return new ResponseEntity<Servicio>(reporteG, HttpStatus.OK);
	}
	/*public Servicio guardarUsuario(@RequestBody(required = true) String usuario, Servicio reporte) throws Exception {
		try {
			return reporteServicio.guardar(usuario, reporte);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Exception(e);
		}
		
	}*/
	
	/*@GetMapping(value = "test")
	public String hi() {
		System.out.println("Call method from client");
		return "Hello from backend";
	}*/

}
