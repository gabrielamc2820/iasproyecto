package com.ias.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ias.modelo.Servicio;

@Repository
public interface ServicioRepositorio extends JpaRepository<Servicio, Long>{
	
	//Servicio save(Servicio s );

}
